/*jslint node: true, nomen: true, plusplus: true*/
'use strict';
// Load required libraries and modules
// External libraries
var _ = require("lodash"),
    async = require('async'),
    config = require('./optimizationConfig.json'),
    env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
    serverConfig = require("../../server/config/config")[env],
    ErrorFactory = require('../errorHandler/errorFactory.js'),
    includes = require('array-includes'),
    logger = require('../config/logger.js'),
    log = logger.LOG,
    utility = require("../utility/utility").Utility,
    utilityObject = new utility(),
    xlsx = require('itt-xlsx'),
    currentHeader, arrayofHeader;


/**
 * function to create the string format of project
 * @param-Json Data
 */

function createDynamicHeader(obj) {
  var ar = [];
  for (var a = 0; a < obj.length; a++) {
    ar.push(obj[a].project + "->" + obj[a].country + "->" + obj[a].site + "->" + obj[a].building + "->" + obj[a].floor);
  }
  return ar;
}

/**
 * function to get Primary headers.We need to add Future state in primary Header.
 * It's not a good approach but right now code is tightly coupled.
 */
function getPrimaryHeader() {
  var primaryArray = Object.keys(config.exportFields);
  primaryArray.splice(3, 0, "future");
  return primaryArray;
}

/**
 * function to get all headers.Instead of creating 2 header Current/Future,
 * we use 1 object current.
 */
function getAllheaders(exportSchemeSet) {
  arrayofHeader = Object.keys(exportSchemeSet.mapping);
  currentHeader = _.cloneDeep(arrayofHeader);
  currentHeader.splice(1, 1);
  return currentHeader.concat(config.exportFields.Recommendation, config.exportFields.RecommendationNotes, arrayofHeader);
}

// Workbook constructor
function Workbook() {
  if (!(this instanceof Workbook)) return new Workbook();
  this.SheetNames = [];
  this.Sheets = {};
}

// create & return Workbook instance
exports.createWorkbook = function () {
  return new Workbook();
};

//function to set primary headers like current state,recommendation,notes and future state
function setPrimaryHeaders(exportData) {
  if (exportData.column >= 0 && exportData.column < (arrayofHeader.length - 1)) {
    exportData.cell = {
      v: config.exportFields.headerMapping.current, s: config.constants.styleBorderFont
    };
  }
  else if (exportData.column === ((arrayofHeader.length - 1) + config.exportFields.Recommendation.length) - 1) {
    exportData.cell = {
      v: config.exportFields.headerMapping.Recommendation, s: config.constants.styleBorderFont
    };
  }
  else if (exportData.column === ((arrayofHeader.length - 1) + config.exportFields.Recommendation.length + config.exportFields.RecommendationNotes.length) - 1) {
    exportData.cell = {
      v: config.exportFields.headerMapping.RecommendationNotes, s: config.constants.styleBorderFont
    };

  }
  else if (exportData.column > ((arrayofHeader.length - 1) + config.exportFields.Recommendation.length + config.exportFields.RecommendationNotes.length) - 1) {
    exportData.cell = {
      v: config.exportFields.headerMapping.future, s: config.constants.styleBorderFont
    };

  }
}

/**
 *function to set macros in coloumn
 * @param data
 * @param exportData
 * @param headerData
 * @param index
 * @param state
 */
function setCurrencyMacros(data, exportData, headerData, index, state) {
  switch (headerData.headers[exportData.column]) {
    case config.constants.BASE_PRICE:
      exportData.cell = {
        t: 'n',
        f: "=ROUND(('" + data[index].country + " in " + data[index].localCurrency + "'!" + config.exportFields.state[state][headerData.headers[exportData.column]] + (exportData.row + 1) + "/'" + config.constants.Conversion_rates + "'!" + config.exportFields.conversionRates[data[index].currencyDescription] + "),2)"
      };
      break;
    case config.constants.MONO_CLICK:
      exportData.cell = {
        t: 'n',
        f: "=ROUND(('" + data[index].country + " in " + data[index].localCurrency + "'!" + config.exportFields.state[state][headerData.headers[exportData.column]] + (exportData.row + 1) + "/'" + config.constants.Conversion_rates + "'!" + config.exportFields.conversionRates[data[index].currencyDescription] + "),5)"
      };
      break;
    case config.constants.COLOR_CLICK:
      exportData.cell = {
        t: 'n',
        f: "=ROUND(('" + data[index].country + " in " + data[index].localCurrency + "'!" + config.exportFields.state[state][headerData.headers[exportData.column]] + (exportData.row + 1) + "/'" + config.constants.Conversion_rates + "'!" + config.exportFields.conversionRates[data[index].currencyDescription] + "),5)"
      };
      break;
    case config.constants.COLOR_PRO_CLICK:
      exportData.cell = {
        t: 'n',
        f: "=ROUND(('" + data[index].country + " in " + data[index].localCurrency + "'!" + config.exportFields.state[state][headerData.headers[exportData.column]] + (exportData.row + 1) + "/'" + config.constants.Conversion_rates + "'!" + config.exportFields.conversionRates[data[index].currencyDescription] + "),5)"
      };
      break;
    default:
      exportData.cell = {v: data[index][config.assets][exportData.index - 3][state][headerData.headers[exportData.column]]};
      break;
  }
}

/**
 *function to fill cell for each floor in  worksheet
 * @param data:json data
 * @param exportData : main object of the worksheet
 * @param headerData:header object of the worksheet
 * @param index : index which depicts floor
 */
function fillFloorData(data, exportData, headerData, index) {
  if (exportData.column >= 0 && exportData.column < (arrayofHeader.length - 1)) {
    if (data[index][config.assets] && exportData.rows > exportData.index && data[index][config.assets][exportData.index - 3][headerData.primaryHeaders[0]]) {
      if (headerData.headers[exportData.column] === config.cost.monthlyCost) {
        exportData.cell = {
          t: 'n',
          f: '=ROUND(SUM(L' + (exportData.row + 1) + '* P' + (exportData.row + 1) + '+M' + (exportData.row + 1) + '*Q' + (exportData.row + 1) + '+O' + (exportData.row + 1) + '+ N' + (exportData.row + 1) + '*R' + (exportData.row + 1) + '),2)'
        };
      }
      else if (data[index].currencyDescription && data[index].country !== config.constants.UNITED_STATES) {
        setCurrencyMacros(data, exportData, headerData, index, config.states.CURRENT);
      }
      else {
        exportData.cell = {v: data[index][config.assets][exportData.index - 3][headerData.primaryHeaders[0]][headerData.headers[exportData.column]]};
      }
    }
    else {
      exportData.cell = {v: null};
    }
  }
  if (exportData.column === (((arrayofHeader.length - 1) + config.exportFields.Recommendation.length) - 1)) {
    if (data[index][config.assets] && exportData.rows > exportData.index) {
      exportData.cell = {v: data[index][config.assets][exportData.index - 3][headerData.primaryHeaders[1]]};
    }
  }

  if (exportData.column === ((arrayofHeader.length - 1) + config.exportFields.Recommendation.length + config.exportFields.RecommendationNotes.length) - 1) {
    if (data[index][config.assets] && exportData.rows > exportData.index) {
      exportData.cell = {v: data[index][config.assets][exportData.index - 3][headerData.primaryHeaders[2]]};
    }
  }
  if (exportData.column > (arrayofHeader.length - 1) + config.exportFields.Recommendation.length + config.exportFields.RecommendationNotes.length - 1) {
    if (data[index][config.assets] && exportData.rows > exportData.index && (data[index][config.assets][exportData.index - 3][headerData.primaryHeaders[3]])) {
      if (headerData.headers[exportData.column] === config.cost.monthlyCost) {
        exportData.cell = {
          t: 'n',
          f: '=ROUND(SUM(AH' + (exportData.row + 1) + '* AL' + (exportData.row + 1) + '+AI' + (exportData.row + 1) + '*AM' + (exportData.row + 1) + '+AK' + (exportData.row + 1) + '+ AJ' + (exportData.row + 1) + '*AN' + (exportData.row + 1) + '),2)',
          s: config.constants.borderRight
        };
      }
      else if (data[index].currencyDescription && data[index].country !== config.constants.UNITED_STATES) {
        setCurrencyMacros(data, exportData, headerData, index,  config.states.FUTURE);
      }
      else {
        exportData.cell = {v: data[index][config.assets][exportData.index - 3][headerData.primaryHeaders[3]][headerData.headers[exportData.column]]};
      }
    }
    else {
      exportData.cell = {v: null};
    }

  }
}

/**
 * process the row and column in sheet
 * @param index : index which depticts the floor
 * @param range : range of worksheet
 * @param exportData : main object of the worksheet
 *@param headerData:header object of the worksheet
 */
function processOptimizationData(index, data, range, exportData, headerData) {
  for (exportData.row = range.s.r, exportData.index = 0; exportData.row < range.e.r, exportData.index <= exportData.rows; ++exportData.row, ++exportData.index) {
    for (exportData.column = range.s.c; exportData.column < range.e.c; ++exportData.column) {
      if (exportData.row === exportData.firstRow) {
        //Setting the project,country,building data
        exportData.cell = {v: headerData.dynamicHeader[index]};
        exportData.cellRef = xlsx.utils.encode_cell({
          c: exportData.column,
          r: exportData.row
        });
        exportData.workSheet[exportData.cellRef] = exportData.cell;
        break;
      }
      else if (exportData.row === (exportData.firstRow + 1)) {
        //setting the primary headers
        setPrimaryHeaders(exportData);

      }
      else if (exportData.row === (exportData.firstRow + 2)) {
        // Set the headers of excel data in cells
        // Add the cells in the row according to dimension
        exportData.cell = {
          v: headerData.headers[exportData.column], s: config.constants.styleBorder
        };
      }
      else if (exportData.row === (range.e.r - 2)) {
        //Set the formula on the last row of every floor
        exportData.cell = {
          v: '',
          s: config.constants.borderTop
        };
        if (exportData.cell.v === null) continue;
      }
      //Setting the Json data into the columns in current and future state
      else {
        // Fill the excel data in cells
        fillFloorData(data, exportData, headerData, index);
        if (exportData.cell.v === null) continue;
        // format number and boolean cells
        if (typeof exportData.cell.v === 'number') exportData.cell.t = 'n';
        else if (typeof exportData.cell.v === 'boolean') exportData.cell.t = 'b';
        else exportData.cell.t = 's';
      }

      // Add the cells in the row according to dimension
      exportData.cellRef = xlsx.utils.encode_cell({
        c: exportData.column,
        r: exportData.row
      });
      exportData.workSheet[exportData.cellRef] = exportData.cell;
    }
    if (exportData.row === (exportData.firstRow + 1)) {
      exportData.mergedCells.push({
            s: {r: (exportData.firstRow + 1), c: 0},
            e: {r: (exportData.firstRow + 1), c: (arrayofHeader.length - 2)}
          },
          {
            s: {
              r: (exportData.firstRow + 1),
              c: (arrayofHeader.length - 1 + config.exportFields.Recommendation.length + config.exportFields.RecommendationNotes.length)
            }, e: {r: (exportData.firstRow + 1), c: headerData.headers.length - 1}
          });
    }
  }
}

/**
 * function to calculate total cost monthly and saving per month
 * @param exportData : range of worksheet
 * @param mainRange : Main range of the worksheet
 */
function calculateTotalCost(exportData, mainRange) {
  exportData.workSheet['Q' + (mainRange.e.r + 1)] = {
    v: config.cost.currentCostPerMoth,
    s: config.constants.fontStyle
  };
  exportData.workSheet['S' + (mainRange.e.r + 1)] = {
    t: 'n',
    f: '=SUM(' + exportData.currentCost.slice(0, -1) + ')',
    s: {font: {bold: true}}
  }
  exportData.workSheet['AI' + (mainRange.e.r + 1)] = {
    v: config.cost.proposedCostPeronth,
    s: config.constants.fontStyle
  };
  exportData.workSheet['AK' + (mainRange.e.r + 1)] = {
    t: 'n',
    f: '=SUM(' + exportData.proposedCost.slice(0, -1) + ')',
    s: {font: {bold: true}}
  }
  exportData.workSheet['Q' + (mainRange.e.r + 3)] = {v: config.cost.savingPerMonth, s: config.constants.fontStyle};
  exportData.workSheet['S' + (mainRange.e.r + 3)] = {
    t: 'n',
    f: '(S' + (mainRange.e.r + 1) + '-AK' + (mainRange.e.r + 1) + ')',
    s: {font: {bold: true}}
  }
  exportData.saving = mainRange.e.r + 3;
}

/**
 * calculate monthly cost for each floor
 * @param exportData : range of worksheet
 * @param range :range of the worksheet
 */
function calculateMonthlyCostPerFloor(exportData, range) {
  exportData.workSheet['D' + (range.e.r - 1)] = {
    t: 'n',
    f: '=COUNTA(D' + exportData.start + ':D' + exportData.end + ')',
    s: config.constants.borderTop
  };
  exportData.workSheet['L' + (range.e.r - 1)] = exportData.cell = {
    t: 'n',
    f: '=SUM(L' + exportData.start + ':L' + exportData.end + ')',
    s: config.constants.borderTop
  }
  exportData.workSheet['M' + (range.e.r - 1)] = exportData.cell = {
    t: 'n',
    f: '=SUM(M' + exportData.start + ':M' + exportData.end + ')',
    s: config.constants.borderTop
  };
  exportData.workSheet['AH' + (range.e.r - 1)] = exportData.cell = {
    t: 'n',
    f: '=SUM(AH' + exportData.start + ':AH' + exportData.end + ')',
    s: config.constants.borderTop
  };
  exportData.workSheet['AI' + (range.e.r - 1)] = exportData.cell = {
    t: 'n',
    f: '=SUM(AI' + exportData.start + ':AI' + exportData.end + ')',
    s: config.constants.borderTop
  };
  exportData.workSheet['S' + (range.e.r - 1)] = exportData.cell = {
    t: 'n',
    f: '=SUM(S' + exportData.start + ':S' + exportData.end + ')',
    s: config.constants.borderTop
  };
  exportData.workSheet['AO' + (range.e.r - 1)] = exportData.cell = {
    t: 'n',
    f: '=SUM(AO' + exportData.start + ':AO' + exportData.end + ')',
    s: config.constants.borderTop
  };

}


/**
 * function to fill the data in cells for  new devices
 * @param data : index which depicts the floor
 * @param exportData : range of worksheet
 * @param headerData :header object
 *  @param newHardwareData:obj for storing new purchased Device
 */
function fillNewDeviceInCells(data, exportData, headerData, newHardwareData) {
  if ((data[newHardwareData.indexDevicePurchased].newHardwarePurchased.length > 0) && headerData.newDeviceHeaders[exportData.column] === 'Qty') {
    exportData.cell = {
      v: newHardwareData.newDevices[Object.keys(newHardwareData.newDevices)[newHardwareData.indexOfNewDevice - 2]]
    };
  }
  else if ((data[newHardwareData.indexDevicePurchased].newHardwarePurchased.length > 0) && headerData.newDeviceHeaders[exportData.column] === 'Make and Model') {
    exportData.cell = {
      v: Object.keys(newHardwareData.newDevices)[newHardwareData.indexOfNewDevice - 2]
    };
  }
  else if ((data[newHardwareData.indexDevicePurchased].newHardwarePurchased.length > 0) && headerData.newDeviceHeaders[exportData.column] === 'Extended Price') {
    exportData.cell = {
      t: 'n',
      f: 'A' + (exportData.row + 1) + '*D' + (exportData.row + 1),
      s: config.constants.borderRight
    };
  }
  else {
    exportData.cell = {
      v: ''
    };
  }
}


//function to fill the data in new purchased device table
function processNewDeviceData(data, exportData, newHardwareData, headerData, outerRange) {
  for (exportData.row = outerRange.s.r, newHardwareData.indexOfNewDevice = 0; exportData.row < outerRange.e.r, newHardwareData.indexOfNewDevice <= newHardwareData.totalNewDevices; ++exportData.row, ++newHardwareData.indexOfNewDevice) {
    for (exportData.column = outerRange.s.c; exportData.column < outerRange.e.c; ++exportData.column) {
      if (exportData.row === newHardwareData.headerOfProposedBill) {
        exportData.cell = {
          v: config.exportFields.proposedBillHeader.proposedBillOfMaterial,
          s: config.constants.styleBorderFont
        };
      }
      else if (exportData.row === (newHardwareData.headerOfProposedBill + 1)) {
        // Set the headers of excel data in cells
        exportData.cell = {
          v: headerData.newDeviceHeaders[exportData.column], s: config.constants.styleBorderFont
        };
      }
      else if (exportData.row === (outerRange.e.r - 1)) {
        //Set the border the last row
        exportData.cell = {
          v: '',
          s: config.constants.borderTop
        };
      }
      else {
        fillNewDeviceInCells(data, exportData, headerData, newHardwareData);

        if (exportData.cell.v === null) continue;
        // Add the cells in the row according to dimension

        // format number and boolean cells
        if (typeof exportData.cell.v === 'number') exportData.cell.t = 'n';
        else if (typeof exportData.cell.v === 'boolean') exportData.cell.t = 'b';
        else exportData.cell.t = 's';
      }
      exportData.cellRef = xlsx.utils.encode_cell({
        c: exportData.column,
        r: exportData.row
      });
      exportData.workSheet[exportData.cellRef] = exportData.cell;
    }
    if (exportData.row === newHardwareData.headerOfProposedBill) {
      exportData.mergedCells.push({
        s: {r: (newHardwareData.headerOfProposedBill), c: 0},
        e: {r: (newHardwareData.headerOfProposedBill), c: 4}
      });
    }

  }
}

/**
 * function to calculate total proposed bill of new devices
 * @param data : index which depicts the floor
 * @param exportData : range of worksheet
 * @param mainRange : main object of the worksheet
 *  @param outerRange:range of new hardware table
 *  @param newHardwareData:obj for storing new purchased Device
 */
function calculateTotalProposedBill(data, exportData, mainRange, outerRange, newHardwareData) {
  if (data[newHardwareData.indexDevicePurchased].newHardwarePurchased.length > 0) {

    exportData.workSheet['D' + (mainRange.e.r + 1)] = {
      v: "Total:",
      s: config.constants.borderTop
    };

    exportData.workSheet['E' + (mainRange.e.r + 1)] = {
      t: 'n',
      f: '=SUM(E' + (outerRange.s.r + 3) + ':E' + (outerRange.e.r - 1) + ')',
      s: config.constants.borderTop
    };
  }
  exportData.workSheet['Q' + (outerRange.s.r)] = {v: config.cost.payBackMonthInPeriod, s: config.constants.fontStyle};
  exportData.workSheet['S' + (outerRange.s.r)] = {
    t: 'n',
    f: '=IF(S' + (exportData.saving) + '= 0, 0, ROUND((E' + (mainRange.e.r + 1) + '/S' + (exportData.saving) + ') ,2))',
    s: {font: {bold: true}}
  };
}

/**
 * process the row and column in sheet
 * @param data : index which depicts the floor
 * @param exportData : range of worksheet
 * @param mainRange : main object of the worksheet
 *@param headerData:header object
 */
function prepareNewPurchasedDeviceData(data, exportData, mainRange, headerData) {
  var newHardwareData = {
    indexOfNewDevice: 0,
    indexDevicePurchased: 0,
    headerOfProposedBill: 0,
    totalNewDevices: 2,
    newDevices: {}
  };
  mainRange.e.r += 6;
  newHardwareData.indexDevicePurchased = data.length - 1;
  newHardwareData.newDevices = _.countBy(data[newHardwareData.indexDevicePurchased].newHardwarePurchased);
  if (data[newHardwareData.indexDevicePurchased].newHardwarePurchased) {
    newHardwareData.totalNewDevices += Object.keys(newHardwareData.newDevices).length;
  }
  //range defined for new purchased device
  var outerRange;
  outerRange = {
    s: {c: 0, r: 0},
    e: {c: (headerData.newDeviceHeaders.length), r: 0}
  };

  outerRange.s.r += mainRange.e.r;
  newHardwareData.headerOfProposedBill = outerRange.s.r;
  mainRange.e.r += newHardwareData.totalNewDevices;
  outerRange.e.r = mainRange.e.r + 1;

  processNewDeviceData(data, exportData, newHardwareData, headerData, outerRange);

  calculateTotalProposedBill(data, exportData, mainRange, outerRange, newHardwareData);
}


// Function to generate a worksheet from the passed JSON data.
exports.generateSheet = function (data, exportSchemeSet) {

  var headerData = {
    dynamicHeader: createDynamicHeader(data),
    headers: getAllheaders(exportSchemeSet),
    primaryHeaders: getPrimaryHeader(),
    newDeviceHeaders: config.exportFields.newHardWarePurchased
  };

  var exportData = {
    workSheet: {},
    mergedCells: [],
    row: 0,
    column: 0,
    firstRow: 0,
    cellRef: '',
    start: 0,
    end: 0,
    rows: 0,
    index: 0,
    currentCost: '',
    proposedCost: '',
    saving: ''
  };

  var range, mainRange = {
    s: {c: 0, r: 0},            //Creating the main range
    e: {c: (headerData.headers.length), r: 0}
  };

  for (var index = 0; index < data.length - 1; index++) {

    // Initialize range of workSheet in cell range object, where 's' is the first cell and 'e' is the last cell in the range
    exportData.rows = 3;
    if (data[index].assets && data[index].assets.length) {
      exportData.rows += data[index].assets.length;
    }
    //initializing the inner range
    range = {
      s: {c: 0, r: 0},
      e: {c: (headerData.headers.length), r: 0}
    };

    //dynamic setting the data based on index
    range.s.r += mainRange.e.r + 1;
    exportData.firstRow = range.s.r;
    mainRange.e.r += exportData.rows + 3;
    range.e.r = mainRange.e.r;
    exportData.start = range.s.r + 4;
    exportData.end = range.e.r - 2;

    // Loop through the rows and columns, and write the values in the sheet
    processOptimizationData(index, data, range, exportData, headerData);
    //calcuating cost  at end of each index
    if (data[index][config.assets]) {
      calculateMonthlyCostPerFloor(exportData, range);
    }
    exportData.currentCost += 'S' + (range.e.r - 1) + ','; //prepare the current cost
    exportData.proposedCost += 'AO' + (range.e.r - 1) + ',';//prepare the proposed cost
  }

  //creating the macros for costing
  calculateTotalCost(exportData, mainRange);
  //saving the value of Saving per month to use in outer range

  //Table for NewPurchasedDevice
  prepareNewPurchasedDeviceData(data, exportData, mainRange, headerData);

  exportData.workSheet['!merges'] = exportData.mergedCells;
  exportData.workSheet['!ref'] = xlsx.utils.encode_range(mainRange);
  exportData.workSheet['!cols'] = config.wscols;
  // Add range of workSheet

  return exportData.workSheet;
};

/**
 * @desc Generation Data & adding it to excel as spread sheet.
 * @param object wb - workbook object
 * @param string workSheetName- title of the spread sheet.
 * @param object countryData - data to be filled in worksheet
 * @return null
 */
exports.addWorksheet = function (wb, workSheetName, countryData, exportMapping) {
  // Add worksheet to workbook
  var workSheet = exports.generateSheet(countryData, exportMapping);
  wb.SheetNames.push(workSheetName);
  wb.Sheets[workSheetName] = workSheet;
};

/**
 * @desc Generate Proposed Material Data for Each Country Sheet.
 * @param array countryData - level and asset data for the country
 * @param array proposedData - Total proposed assets for the project.
 * @return object
 */
exports.getProposedMaterial = function (countryData, proposedData) {
  if (!proposedData.length)
    return {newHardwarePurchased: []};
  else {
    var futureAssets = {}, proposedAssets = [];
    countryData.forEach(function (data) { //loop per the countries in project
      if (data && data.levelID) {
        futureAssets[data.levelID] = true;
      }
    });
    for (var index in proposedData) { // loop per the future assets in project
      if (proposedData[index].levelID in futureAssets) {
        proposedAssets.push(proposedData[index].proposedAsset);
      }
    }
    return {newHardwarePurchased: proposedAssets};
  }
};
