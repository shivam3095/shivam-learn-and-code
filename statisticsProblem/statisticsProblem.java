import java.util.*;

class Statistics 
{
	static HashMap<String,String>personNameWithSportsName = new HashMap<>();
	static HashMap<String,Integer>sportsNameWithItsPopularity = new HashMap<>();
	
	public static void main(String args[] ) 
	{
		Scanner scan = new Scanner (System.in);
		String favSportName="";
		int totalNumberOfEntries=scan.nextInt();
		String name,sport;
		for (int entries=0; entries < totalNumberOfEntries; entries++)
		{
			name = scan.next();
			sport = scan.next();
			AddingRecordInMap(name, sport);
		}
		favSportName=EvaluateMaximumPopularityOfSport();
		
		System.out.println(favSportName);
		System.out.println(sportsNameWithItsPopularity.get("football")==null?"0":sportsNameWithItsPopularity.get("football"));
	}  
	public static void AddingRecordInMap(String name, String sport)
	{
		if(!personNameWithSportsName.containsKey(name))
		{
			if(!sportsNameWithItsPopularity.containsKey(sport)) 
			{
				sportsNameWithItsPopularity.put(sport,1);
			}
			else 
			{
				sportsNameWithItsPopularity.put(sport,sportsNameWithItsPopularity.get(sport)+1);
			}
		}
		personNameWithSportsName.put(name,sport);
	}
	public static String EvaluateMaximumPopularityOfSport()
	{
		int maxNumberOfFans=0;
		String favSportName="";
		Set<String> sportNames = sportsNameWithItsPopularity.keySet();
		for (String sportname : sportNames)
		{
			int likers = sportsNameWithItsPopularity.get(sportname);
			if (likers > maxNumberOfFans) 
			{
				maxNumberOfFans = likers;
				favSportName = sportname;
			}
		}
		return favSportName;
	}
}