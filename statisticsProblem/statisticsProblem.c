#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define variableForHashIndex 7
static int mostPopularSportCount=0;
struct personSportPair
{
	int keyOfEntryForInsertion;
	char name [11];
	char sport [11];
};

struct sportPopulairty
{
	char sport [11];
	int sportCount;
};

struct personSportPair* hashArrayOfPersonSportPair[100];
struct sportPopulairty* hashArrayOfSportPopularity[100];
struct sportPopulairty* dummyItem;
struct personSportPair* item;

int hashCode(int key)
{
	return key % variableForHashIndex;
}

void insert(int keyOfEntryForInsertion,char name [11] , char sport [11])
{
	struct personSportPair *item = (struct personSportPair*) malloc(sizeof(struct personSportPair));
	strcpy(item->name, name);
	strcpy(item->sport, sport);
	item->keyOfEntryForInsertion = keyOfEntryForInsertion;
	int hashIndex = hashCode(keyOfEntryForInsertion);
	while(hashArrayOfPersonSportPair[hashIndex] != NULL)
	{
		++hashIndex;
		hashIndex %= variableForHashIndex;
	}
	hashArrayOfPersonSportPair[hashIndex] = item;
	return ;
}

void insertSportandPopularity(int keyOfSportForInsertion,char sport [11],int sportCount)
{
	struct sportPopulairty *dummyItem = (struct sportPopulairty*) malloc(sizeof(struct sportPopulairty));
	strcpy(dummyItem->sport, sport);
	dummyItem->sportCount=sportCount;
	int hashIndex = hashCode(keyOfSportForInsertion);
	while(hashArrayOfSportPopularity[hashIndex] != NULL)
	{
		++hashIndex;
		hashIndex %= variableForHashIndex;
	}
	hashArrayOfSportPopularity[hashIndex] = dummyItem;
	return ;
}

struct personSportPair *searchForExistingEntry(int keyOfEntryForInsertion,char name [11] , char sport [11])
{
	int hashIndex = hashCode(keyOfEntryForInsertion);  	
	while(hashArrayOfPersonSportPair[hashIndex] != NULL)
	{
		if(hashArrayOfPersonSportPair[hashIndex]->name == name)
		return NULL; 			
		++hashIndex;
		hashIndex %= variableForHashIndex;
	}        
	insert(keyOfEntryForInsertion,name,sport);
	return hashArrayOfPersonSportPair[hashIndex];        
}

char *searchForExistingSport(int keyOfSportForInsertion,char sport [11]) {
	int hashIndex = hashCode(keyOfSportForInsertion);  
	char *mostPopularSport;
	while(hashArrayOfSportPopularity[hashIndex] != NULL) 
	{
		if(strcmp(hashArrayOfSportPopularity[hashIndex]->sport,sport)==0)
		{
			hashArrayOfSportPopularity[hashIndex]->sportCount=hashArrayOfSportPopularity[hashIndex]->sportCount+1;
			if(hashArrayOfSportPopularity[hashIndex]->sportCount>mostPopularSportCount)
			{
				mostPopularSportCount = hashArrayOfSportPopularity[hashIndex]->sportCount;
				strcpy(mostPopularSport,hashArrayOfSportPopularity[hashIndex]->sport);
				return mostPopularSport;
			}
			return mostPopularSport;
		}		
		++hashIndex;	
		hashIndex %= variableForHashIndex;
	}        
	insertSportandPopularity(keyOfSportForInsertion,sport,1);
	if(hashArrayOfSportPopularity[hashIndex]->sportCount>mostPopularSportCount)
	{
		mostPopularSportCount = hashArrayOfSportPopularity[hashIndex]->sportCount;
		strcpy(mostPopularSport,hashArrayOfSportPopularity[hashIndex]->sport);
		return mostPopularSport;
	}
	return mostPopularSport;      
}

int main()
{
	int increamentVariable,sizeOfTotalEntries,keyOfEntryForInsertion,keyOfSportForInsertion,personsLikeFootball=0;
	char name [11],*mostPopularSport;
	char sport [11];
	scanf("%d",&sizeOfTotalEntries);
	for(increamentVariable=0;increamentVariable<sizeOfTotalEntries;increamentVariable++)
	{  
		scanf("%s", name);
		scanf("%s", sport);
		keyOfEntryForInsertion = strlen(name) + strlen(sport);
		keyOfSportForInsertion = strlen(sport);
		if(searchForExistingEntry(keyOfEntryForInsertion,name,sport))
		{	
			if(strcmp(sport,"football")==0)
			{
				personsLikeFootball++;
			}
			mostPopularSport=searchForExistingSport(keyOfSportForInsertion,sport);
		}
	}
	printf("%s",mostPopularSport);
	printf("\n%d",personsLikeFootball);
	return 0;
}
