#include<stdio.h>
#include<stdlib.h>

struct Node 
{
	int data;
	struct Node *left, *right;
};

struct Node *newNode(int data)
{
	struct Node* addressOfNewNode = (struct Node*)malloc(sizeof(struct Node));
	addressOfNewNode->data = data;
	addressOfNewNode->left = addressOfNewNode->right = NULL;
	return addressOfNewNode;
}

int heightOfTree(struct Node* node) 
{
	if (node==NULL) 
	return 0;
	else
	{
		int heightOfLeftSubTree = heightOfTree(node->left);
		int heightOfRightSubTree = heightOfTree(node->right);
		if (heightOfLeftSubTree > heightOfRightSubTree)
		{
			return(heightOfLeftSubTree+1);
		}
		else 
		return(heightOfRightSubTree+1);
	}
} 

struct Node* insertNewNodeInTree(struct Node* node, int data)
{
	if (node == NULL) 
	return newNode(data);
	if (data < node->data)
	{
		struct Node *addressOfLeftChild = insertNewNodeInTree(node->left, data);
		node->left = addressOfLeftChild;
	}
	else if (data > node->data)
	{
		struct	Node *addressOfRightChild = insertNewNodeInTree(node->right, data);
		node->right = addressOfRightChild;
	}
	return node;
}

int main()
{
	
	struct Node *addressOfCurrentNode = NULL;
	struct Node *addressOfParentNode = NULL;
	int totalNumber,index;
	scanf("%d", &totalNumber);
	int inputOfIntegers[totalNumber];
	for(index=0;index<totalNumber;index++)
	{
		scanf("%d",&inputOfIntegers[index]);
	}
	addressOfCurrentNode = insertNewNodeInTree(addressOfCurrentNode, inputOfIntegers[0]);
	addressOfParentNode = addressOfCurrentNode;
	for(index=1;index<totalNumber;index++)
	{
		insertNewNodeInTree(addressOfCurrentNode, inputOfIntegers[index]);
	}
	printf("%d", heightOfTree(addressOfParentNode));
	return 0;
}
