#include<stdio.h>
#include<stdlib.h>
 
struct nodeOfFriend
{	
	int popularityOfFriend;
	struct nodeOfFriend *nextFriend;
};
 
struct nodeOfFriend* createNodeOfNewFriend(int popularity)
{
	struct nodeOfFriend* newFriend;
	newFriend = (struct nodeOfFriend*) malloc(sizeof(struct nodeOfFriend));
	newFriend->popularityOfFriend = popularity;
	newFriend->nextFriend = NULL;
	return newFriend;
}

struct nodeOfFriend* insertionToFriendsList(struct nodeOfFriend* firstPosition, struct nodeOfFriend* newFriend)
{
	if(firstPosition)
		newFriend->nextFriend = firstPosition;
	return newFriend;
}
 
struct nodeOfFriend* deletionOfFriendOfLessPopularity(struct nodeOfFriend *firstPosition)
{
	if(firstPosition == NULL)
		return firstPosition;
	struct nodeOfFriend *nextToTop = firstPosition->nextFriend;
	free(firstPosition);
	return nextToTop;
}
 
void displayListOfRemainingFriends(struct nodeOfFriend* headNode)
{
	if(headNode == NULL)
		return;
	displayListOfRemainingFriends(headNode->nextFriend);
	printf("%d ", headNode->popularityOfFriend);
}
 
int main()
{
	int testCases,totalNumberOfFriends,numberOfFriendsToDelete,loopOfTestCases,loopOfTotalNoOfFriends,popularityOfFriend;
	struct nodeOfFriend *start = NULL, *nodeOfNewFriend;
	scanf("%d", &testCases);
	for(loopOfTestCases=0; loopOfTestCases<testCases; loopOfTestCases++)
	{
		scanf("%d %d", &totalNumberOfFriends, &numberOfFriendsToDelete);
		start = NULL;
		for(loopOfTotalNoOfFriends=0; loopOfTotalNoOfFriends<totalNumberOfFriends; loopOfTotalNoOfFriends++)
		{
			scanf("%d", &popularityOfFriend);
			nodeOfNewFriend = createNodeOfNewFriend(popularityOfFriend);
			while(start!=NULL && start->popularityOfFriend < popularityOfFriend && numberOfFriendsToDelete)
			{
				start = deletionOfFriendOfLessPopularity(start);    
				numberOfFriendsToDelete--;
			}
			start = insertionToFriendsList(start, nodeOfNewFriend);    
		}
		displayListOfRemainingFriends(start);
		printf("\n\n");
	}
	return 0;
}
