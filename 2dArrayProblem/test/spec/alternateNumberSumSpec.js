'use strict';
var chai = require('chai'),
    expect = chai.expect,
    sumOfAlternateNumbers = require('../../alternateNumberSum.js'),
    testData = require('../fixtures/alternateNumberSumTestData.json');

describe('Testing Alternate Number Sum Module', function () {
  it('Should return sum of even numbers', function (done) {
    sumOfAlternateNumbers.sumOfEvenNumbers(testData.matrix, function (err, result) {
      expect(result).to.be.equal(25);
      expect(result).not.to.be.null;
      expect(err).to.be.null;
      done();
    });
  });
  it('Should return err as invalid input is provided', function (done) {
    sumOfAlternateNumbers.sumOfEvenNumbers(null, function (err, result) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Invalid input is provided");
      done();
    });
  });
  it('Should return err as input of invalid length is provided', function (done) {
    sumOfAlternateNumbers.sumOfEvenNumbers(testData.invalidMatrix, function (err, result) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Input length should be nine");
      done();
    });
  });
  it('Should return sum of odd numbers', function (done) {
    sumOfAlternateNumbers.sumOfOddNumbers(testData.matrix, function (err, result) {
      expect(result).to.be.equal(20);
      expect(result).not.to.be.null;
      expect(err).to.be.null;
      done();
    });
  });
  it('Should return err as invalid input is provided', function (done) {
    sumOfAlternateNumbers.sumOfOddNumbers(null, function (err, result) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Invalid input is provided");
      done();
    });
  });
  it('Should return err as input of invalid length is provided', function (done) {
    sumOfAlternateNumbers.sumOfOddNumbers(testData.invalidMatrix, function (err, result) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Input length should be nine");
      done();
    });
  });
  it('Should return object contains sum of odd numbers and even numbers ', function (done) {
    sumOfAlternateNumbers.findSum(testData.matrix, function (err, result) {
      expect(err).to.be.null;
      expect(result).instanceof(Object);
      expect(result.sumOfEvenNumbers).to.be.equal(25);
      expect(result.sumOfOddNumbers).to.be.equal(20);
      done();
    });
  });
  it('Should return err as invalid input is provided', function (done) {
    sumOfAlternateNumbers.findSum(null, function (err, result) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Invalid input is provided");
      done();
    });
  });
  it('Should return err as input of invalid length is provided', function (done) {
    sumOfAlternateNumbers.findSum(testData.invalidMatrix, function (err, result) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Input length should be nine");
      done();
    });
  });
});