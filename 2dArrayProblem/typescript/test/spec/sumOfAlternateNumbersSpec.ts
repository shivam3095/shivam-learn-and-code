import "mocha";
import {expect} from "chai";

let sumOfAlternateNumber = require("../../sumOfAlternateNumbersOf2dArray").AddAlternateElements,
    sumOfAlternateNumbers = new sumOfAlternateNumber(),
    testData = require("../fixtures/alternateNumberSumTestData.json");

describe("Testing Alternate Number Sum Module", function () {
  it("Should return 2d array", function (done: Function) {
    sumOfAlternateNumbers.pocessInput(testData.validInput, function (err: any, result: any) {
      expect(result).to.be.instanceof(Array);
      expect(result).not.to.be.null;
      expect(err).to.be.null;
      done();
    });
  });
  it("Should return err as invalid input is provided", function (done: Function) {
    sumOfAlternateNumbers.pocessInput(null, function (err: any, result: any) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Invalid input is provided");
      done();
    });
  });
  it("Should return err as input of invalid length is provided", function (done: Function) {
    sumOfAlternateNumbers.pocessInput(testData.invalidInput, function (err: any, result: any) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Input length should be nine");
      done();
    });
  });
  it("Should return err as input is invalid i.e. String", function (done: Function) {
    sumOfAlternateNumbers.pocessInput("String", function (err: any, result: any) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Invalid input is provided");
      done();
    });
  });

  it("Should return sum of alternate numbers", function (done: Function) {
    sumOfAlternateNumbers.calculateSumOfAlternateNumbers(testData.validInput, testData.positionToStart, function (err: any, result: any) {
      expect(result).to.be.instanceof(Object);
      expect(result).not.to.be.null;
      expect(result).not.to.be.empty;
      expect(result.odd).to.equal(25);
      expect(result.even).to.equal(20);
      expect(err).to.be.null;
      done();
    });
  });
  it("Should return err as invalid input is provided", function (done: Function) {
    sumOfAlternateNumbers.calculateSumOfAlternateNumbers(null, testData.positionToStart, function (err: any, result: any) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Invalid input is provided");
      done();
    });
  });
  it("Should return err as invalid input is provided", function (done: Function) {
    sumOfAlternateNumbers.calculateSumOfAlternateNumbers(testData.invalidInput, testData.positionToStart, function (err: any, result: any) {
      expect(result).to.be.null;
      expect(err).to.be.equal("Input length should be nine");
      done();
    });
  });
});