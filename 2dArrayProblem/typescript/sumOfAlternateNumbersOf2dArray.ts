import {ISumOfAlternateNumbersOf2dArray} from "./iSumOfAlternateNumersOf2dArray";

export class AddAlternateElements implements ISumOfAlternateNumbersOf2dArray {

  /**
   * Process input to 2d array
   * @param sampleInput: number
   * @param next: callback method
   */
  pocessInput(sampleInput: number[], next: Function) {
    return next("Implementation has not been done yet", null);
  }

  /**
   * Calculate sum of alternate numbers
   * @param matrix: number
   * @param positionToStart: number[][]
   * @param next: callback method
   */
  calculateSumOfAlternateNumbers(matrix: number[][], positionToStart: number[][], next: Function) {
    return next("Implementation has not been done yet", null);
  }
}