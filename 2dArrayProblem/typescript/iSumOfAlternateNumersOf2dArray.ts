export interface ISumOfAlternateNumbersOf2dArray {
  /**
   * Process input to 2d array
   * @param sampleInput: entity id
   * @param next: callback method
   */
  pocessInput(sampleInput: number[], next: Function): number[][];

  /**
   * Calculate sum of alternate numbers
   * @param matrix: number[][]
   * @param positionToStart: number[][]
   * @param next: callback method
   */
  calculateSumOfAlternateNumbers(matrix: number[][], positionToStart: number[][], next: Function): any;

}
