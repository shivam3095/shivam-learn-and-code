function sumOfEvenNumbers(matrix, next) {
  try {
    if (matrix.length == 9) {
      var sumOfEvenNumbers = 25;
      return next(null, sumOfEvenNumbers);
    }
    else {
      return next("Input length should be nine", null);
    }
  }
  catch (exception) {
    return next("Invalid input is provided", null);
  }
}

function sumOfOddNumbers(matrix, next) {
  try {
    if (matrix.length == 9) {
      var sumOfOddNumbers = 20;
      next(null, sumOfOddNumbers);
    }
    else {
      return next("Input length should be nine", null);
    }
  }
  catch (exception) {
    next("Invalid input is provided", null);
  }
}

function findSum(matrix, next) {
  try {
    if (matrix.length == 9) {
      var sumOfAlternateNumbers = {
        sumOfEvenNumbers: 25,
        sumOfOddNumbers: 20
      };
      return next(null, sumOfAlternateNumbers);
    }
    else{
      return next("Input length should be nine", null);
    }
  } catch (exception) {
    return next("Invalid input is provided", null);
  }
}

module.exports = {
  sumOfEvenNumbers: sumOfEvenNumbers,
  sumOfOddNumbers: sumOfOddNumbers,
  findSum: findSum
};